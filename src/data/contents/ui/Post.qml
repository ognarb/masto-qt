import QtQuick 2.1
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

RowLayout {
    
    Image {
        Layout.fillWidth: true
        Layout.minimumWidth: 33
        Layout.maximumWidth: 33
        Layout.minimumHeight: 33
        Layout.maximumHeight: 33
        source: "carl.png"
    }
    
    Text {
        Layout.fillWidth: true
        Layout.minimumWidth: 400
        Layout.maximumWidth: 400
        Layout.minimumHeight: 150
        wrapMode: Text.Wrap
        text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
    }
}
